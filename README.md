# JavaScript Challenge Video
Challenge for jobsity

This is the package list, for the challenge

"prop-types": "^15.6.2",
"react": "^16.4.1",
"react-bootstrap": "^0.32.1",
"react-dom": "^16.4.1",
"react-redux": "^5.0.7",
"react-scripts": "1.1.4",
"redux": "^4.0.0",
"redux-persist": "^5.10.0"

# Install dependencies
yarn install

# To run the apps

yarn start

Now you can see the project run in this url

http://localhost:3000
