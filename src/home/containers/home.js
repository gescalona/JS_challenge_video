import React, { Component } from 'react'
import HandleError from '../../errors/containers/handle-errors'
import VideoPlayer from '../../player/containers/player'
import '../components/main-layout.css'
import { connect } from 'react-redux'

class Home extends Component {

  render() {
    return(
      <HandleError>
        <div style={{minHeight: '100vh', display: 'flex', flexDirection: 'column'}}>


        <div className="toolbar">
          <img src="images/logo-clips.png" alt="" className="logo"/>
        </div>
        <VideoPlayer
        //videos={this.props.videos}
        />
        </div>
      </HandleError>
    )
  }
}


function mapStateToProps(state, props) {
  return {
    videos: state.data.videos,
  }
}

export default connect(mapStateToProps)(Home)
