// import data from './data'
// import modal from './modal'
// import {combineReducers} from 'redux'


// const rootReducer = combineReducers({
//   data,
//   modal
// })

// export default rootReducer


import { createStore, applyMiddleware, compose, combineReducers} from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web and AsyncStorage for react-native

import data from './data'
import modal from './modal'

const reducers = combineReducers({
  	data,
  	modal
})

const persistConfig = {
  key: 'video_challenge',
  storage,
}

const persistedReducer = persistReducer(persistConfig, reducers)

export default () => {
  const store = createStore(persistedReducer, undefined);
  let persistor = persistStore(store)
  return { store, persistor }
}