const initialState = {
  visibility: false,
}

function modal(state = initialState, action){
  switch (action.type) {
    case 'OPEN_CLOSE_MODAL':
        return {
          ...state,
          visibility: !state.visibility
        }
      break
    default:
      return state

  }
}

export default modal
