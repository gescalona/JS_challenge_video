import React from 'react'
import { render } from 'react-dom';
import './index.css'
import Home from './home/containers/home'
import {Provider} from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { createStore } from 'redux'
// import reducer from './reducers/index'
import configureStore from './reducers'

// import data from './data/video-data.json'
import registerServiceWorker from './registerServiceWorker'
// const store = createStore(
//   reducer,
//   {},
//   window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
// )
const {store, persistor} = configureStore()
const root = document.getElementById('root')
render(
        <Provider store={store}>
        	<PersistGate persistor={persistor} >
          		<Home />
          	</PersistGate>
        </Provider>,
        root
      )
registerServiceWorker()
