import React, {Component} from 'react'
import VideoPlayerView from '../components/video-player-view'
import Video from '../components/video'
import VideoList from './video-list'
import {connect} from 'react-redux'

class Player extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }
  handleLoadedMetaData = event => {
  }
  handleUpdateMetaData = event =>{
    this.video = event.target
    if(this.state.currentClip){
      // console.log(this.video.currentTime, this.state.currentClip.end_time)
    }

    if(this.state.currentClip&&(this.video.currentTime>=this.state.currentClip.end_time)){
      this.onFinishCurrentVideo()
    }
    // this.setState({
    //   currenttime: (this.video.currentTime)
    // })
  }

  handlePlayClip = event => {
    console.log(event.target.id)
    this.clip = event.target.id.split('-')
    let resultado
    let src = ''
    let currentVideo
    this.props.videos.map((video) =>{
      if(video.id === this.clip[0]){
        currentVideo = video
        src = video.src
        return resultado = video.clips.find( clips => clips.id === this.clip[1] );
      }
    })
    this.setState({
      currentVideo,
      currentClip: resultado
    })
    this.video.src = src+'#t=' +resultado.start_time +','+resultado.end_time
    this.video.play()
  }
  onFinishCurrentVideo=()=>{
    let resultado
    let src = ''
    this.props.videos.map((video) =>{
      if(video.id === this.state.currentVideo.id){
        src = video.src
        let found = false
        video.clips.map((item,i)=>{
          if(found===true){
            resultado = item
          }
          if(this.state.currentClip.id===item.id){
            found = true
          }
        })
      }
    })
    if(resultado){
      this.setState({
        currentClip: resultado
      })
      this.video.src = src+'#t=' +resultado.start_time +','+resultado.end_time
      this.video.play()
    }

  }
  setRef = element => {
    this.video = element
  }

  render() {
    return(
      <VideoPlayerView>
        <div className="container">
          <div className="row-cont" style={{}}>
          <div className="content-flex" style={{backgroundImage: "url('images/background.png')", minHeight: window.innerHeight-110}}>
            <div style={{maxWidth: 800, width: '100%'}}>
              <Video
              autoplay={false}
              src={'https://download.blender.org/durian/trailer/sintel_trailer-480p.mp4'}
              handleLoadedMetaData={this.handleLoadedMetaData}
              handleUpdateMetaData={this.handleUpdateMetaData}
              setRef={this.setRef}
              />
            </div>

          </div>
          <div className="side-bar">
            <VideoList
            currentVideo={this.state.currentVideo}
            currentClip={this.state.currentClip}
            videos={this.props.videos}
            handlePlayClip={this.handlePlayClip}
            />
          </div>
          </div>
        </div>
      </VideoPlayerView>
    )
  }
}

function mapStateToProps(state, props){
  return {
    videos: state.data.dataAPI.videos
  }
}

export default connect(mapStateToProps)(Player)
