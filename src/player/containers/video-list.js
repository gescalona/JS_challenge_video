import React, { Component } from 'react'
import VideoListLayout from '../components/video-list-layout'
import Modal from '../components/modal'
import '../components/video-list.css'
import {connect} from 'react-redux'
import { openModal } from '../../actions/modalActions'
import { deleteClip } from '../../actions/clipAction'

class VideoList extends Component {
  state = {
    openModal: true,
    videoData: {
      src: 'https://download.blender.org/durian/trailer/sintel_trailer-480p.mp4',
      name:'Borrar el initial SRTATE del moda;',
      parent_id: '1'
    }
  }

  handleAddClip = event => {
    this.setState({
      videoData: {
        src: event.target.getAttribute("data-video"),
        name: event.target.getAttribute("data-name"),
        parent_id: event.target.id,
      }
    })
    this.handleOpenModal()
  }

  handleOpenModal = event => {
      this.props.dispatch(openModal())
  }

  handleDeleteClip = clip => {
    this.props.dispatch(deleteClip(clip.target.id.substring(2, clip.target.id.length),
                                   clip.target.getAttribute("data-parent")
                                   )
                        )
    //This is a bad hack, because the list not render the new data
    this.handleOpenModal()
    this.handleOpenModal()
  }

  handleEditClip =(video, clip)=> {
    // console.log(video, clip);
    this.setState({
      videoData: {
        src: video.src,
        name: video.name,
        parent_id: video.id,
        currentClip: clip
      }
    })
    this.handleOpenModal()
  }

  render() {
    return(
      <VideoListLayout>
          <div style={{width: 320, display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column'}}>
            <p className="font-bold" style={{fontSize: 20}}>
              Videos & Clips list
            </p>
            <br/>
            {
                this.props.videos.map((item) =>
                  <div key={item.id} style={{width: '100%'}}>
                      <div className="row-cont" style={{justifyContent: 'space-between'}}>
                        <p className="font-bold" style={{color: '#808080', fontSize: 16}}>
                          {item.name}
                        </p>
                        <img
                        id={item.id}
                        onClick={this.handleAddClip}
                        data-video={item.src}
                        data-name={item.name}
                        src="images/add.png" className='clipAction opacity' alt=""/>
                      </div>
                    <div className="ClipChildren">
                      {
                        item.clips.map((clip) => (
                          <div
                          className="ClipChildren2"
                          key={'clip_'+clip.id}>
                            <p className='clipName'>{clip.name}</p>
                            <div className="row-cont">
                              <img
                              id={item.id + '-' + clip.id}
                              onClick={this.props.handlePlayClip}
                              src="images/play.png" className='clipAction opacity' alt=""/>
                              <img
                              data-video={item.src}
                              data-name={item.name}
                              data-parent={item.id}
                              data-clip={clip}
                              onClick={()=>this.handleEditClip(item,clip)}
                              src="images/edit.png"
                              className='clipAction opacity' alt=""/>
                              <img
                              onClick={this.handleDeleteClip}
                              id={'d_'+clip.id}
                              data-parent={item.id}
                              src="images/delete.png"
                              className='clipAction opacity' alt=""/>
                            </div>
                          </div>
                        ))
                      }
                    </div>
                  </div>
                )
              }
          </div>
          {
            this.props.openModal &&
              <Modal
              currentClip={this.state.videoData.currentClip}
              openModal={this.handleOpenModal}
              videoData={this.state.videoData}
              handleSaveClip={this.handleSaveClip}
              />
          }
        </VideoListLayout>
      )
  }
}

function mapStateToProps(state, props){

  return {
    videos: state.data.dataAPI.videos,
    openModal: state.modal.visibility
  }
}

export default connect(mapStateToProps)(VideoList)
