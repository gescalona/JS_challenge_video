import React from 'react'
import {ListGroup, ListGroupItem} from 'react-bootstrap'
import {ButtonToolbar, Button, Grid, Row, Col, Glyphicon, Table } from 'react-bootstrap'
import './video-of-list.css'
function VideoOfList(props) {
  return(
        <tbody>
        <tr>
          <td>
            {props.name}
          </td>
          <td>


          </td>
        </tr>
        <tr>
          <td colSpan="4">
          <Table style={{width: "100%"}}>
            <tbody>
              {
                props.clips.map((clip) => (
                  <tr key={clip.id}>
                    <td id={props.id + '-' + clip.id} onClick={props.handleOnClipClick} >
                        {clip.name}
                    </td>
                    <td className="text-right">
                      <Button key={clip.id} bsStyle="danger" bsSize="xsmall" >
                        <Glyphicon glyph="trash" />
                      </Button>
                    </td>
                  </tr>
                ))
              }
            </tbody>
          </Table>
          </td>
          </tr>

      </tbody>
  )
}
export default VideoOfList
